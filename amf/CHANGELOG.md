### 1.1.3
1.本库是基于amf-convert源库v1.1.3版本代码进行OpenHarmony环境接口验证的三方库。

2.amf-convert在javascript环境中可以实现AMF格式的serialization/deserialization。

3.amf-convert目前支持规格:Undefined,Null,False,True,Integer,Double,String,Date,Array,Object,ByteArray。

