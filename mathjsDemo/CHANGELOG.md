## v2.0.0
- 适配DevEco Studio 版本：4.0 Beta2（4.0.3.411），OpenHarmony SDK:API10（4.0.10.2）

## 0.1.1
1.适配DevEco Studio 3.1 Beta1版本。

## 0.1.0
1.支持数字、大数、三角函数、字符串、和矩阵等数学运算功能。
