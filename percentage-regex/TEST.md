## validator单元测试用例

该测试用例基于OpenHarmony系统下，采用[percentage-regex库测试用例](https://github.com/arthurvr/percentage-regex/blob/master/test.js) 采用进行单元测试

### 单元测试用例覆盖情况
percentage-regex

| 接口                         | 是否通过 | 备注  |
|----------------------------|------|-----|
| percentageRegex(options)   | pass |     |
| options.exact              | pass |     |

