[
    {
        "Name": "asn1",
        "License": "MIT License",
        "License File": "https://github.com/ComplyCloud/asn1/blob/master/LICENSE",
        "Version Number": "v0.3.4",
        "Owner":"ComplyCloud",
        "Upstream URL": "https://github.com/ComplyCloud/asn1",
        "Description": "Javascript library for Abstract Syntax Notation One (ASN.1)"
    }
	 {
        "Name": "asn1-der",
        "License": "MIT License",
        "License File": "https://github.com/ComplyCloud/asn1-der/blob/master/LICENSE",
        "Version Number": "v0.1.0",
        "Owner":"ComplyCloud",
        "Upstream URL": "https://github.com/ComplyCloud/asn1-der",
        "Description": "Javascript library for DER and PEM serialization/deserialization of Abstract Syntax Notation One (ASN.1)."
    }
	 {
        "Name": "asn1js",
        "License": "Copyright (c) 2014, GMO GlobalSign Copyright (c) 2015-2022, Peculiar Ventures All rights reserved.",
        "License File": "https://github.com/PeculiarVentures/ASN1.js/blob/master/LICENSE",
        "Version Number": "3.0.5",
        "Owner":"PeculiarVentures",
        "Upstream URL": "https://github.com/PeculiarVentures/asn1.js",
        "Description": "ASN1js is a pure JavaScript library implementing a full ASN.1 BER decoder and encoder."
    }
	 {
        "Name": "Base64",
        "License": "Apache License",
        "License File": "https://github.com/davidchambers/Base64.js/blob/master/LICENSE",
        "Version Number": "v1.1.0",
        "Owner":"davidchambers",
        "Upstream URL": "https://github.com/davidchambers/Base64.js",
        "Description": "Polyfill for browsers that don't provide window.btoa and window.atob"
    }
	 {
        "Name": "hex-encode-decode",
        "License": "MIT License",
        "License File": "https://github.com/tiaanduplessis/hex-encode-decode/blob/master/LICENSE",
        "Version Number": "v1.0.0",
        "Owner":"tiaanduplessis",
        "Upstream URL": "https://github.com/tiaanduplessis/hex-encode-decode",
        "Description": "Hex encode & decode string."
    }
]