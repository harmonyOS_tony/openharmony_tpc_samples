[

    {
        "Name":"js-adler32",
        "License":"Apache License 2.0",
        "License File":"https://github.com/SheetJS/js-adler32/blob/master/LICENSE",
        "Version Number":"v1.3.1",
        "Owner":"SheetJS",
        "Upstream URL":"https://github.com/SheetJS/js-adler32",
        "Description":"Signed ADLER-32 algorithm implementation in JS (for the browser and nodejs). Emphasis on correctness, performance, and IE6+ support."
    }

]