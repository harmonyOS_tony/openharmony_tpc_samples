## 2.0.1
1.适配DevEco Studio 版本: 4.0(4.0.3.512),SDK: API10（4.0.10.9）
2.适配ArkTs语法

## 2.0.0
1.适配DevEco Studio:3.1 Beta2(3.1.0.400), SDK:API9 Release(3.2.11.9)
2.包管理工具由npm切换成ohpm
3.补充组件接口XTS测试

## 0.2.1
1.适配DevEco Studio 3.1 Beta1版本。

## 0.2.0
1.eventbusjs 0.2.0版本组件的Demo，验证消息订阅发送功能。