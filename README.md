<h1 align="center">三方组件sample汇总</h1> 

##### 本文收集了一些可以在openharmony中直接使用的三方组件的sample资源，欢迎应用开发者参考和使用，同时也欢迎开发者增加三方组件的sample，可以提PR加入到列表当中

## 目录
- [工具](#工具)
- [三方组件sample](#三方组件sample)
    - [文件数据](#文件数据)
        - [文件解析](#文件解析)
        - [编码解码](#编码解码)
    - [工具库](#工具库)
        - [数学计算](#数学计算)
        - [加解密算法](#加解密算法)
    - [框架](#框架)
    - [其他](#其他)
  
## 工具
- [IDE官方下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio) - DevEco Studio

[返回目录](#目录)

## <a name="三方组件sample"></a>三方组件sample

### <a name="文件数据"></a>文件数据

#### <a name="文件解析"></a>文件解析
- [xml2jsDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/xml2jsDemo) - 基于开源库node-xml2js开发OpenHarmony的demo例子，XML到JavaScript对象转换器。

[返回目录](#目录)

#### <a name="编码解码"></a>编码解码
- [protobuf](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/protobuf) - 基于开源库protobufjs开发OpenHarmony的demo例子，主要功能是序列化和反序列化。
- [commons_codec](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/commons_codec) - 基于开源库caverphone，base32，base64，MD2，MD5，SHA1，SHA256，metaphonesoundex等库开发OpenHarmony的demo例子，包含各种格式的简单编码器和解码器。
- [cborjsDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/cborjsDemo) - 基于开源库cbor-js开发OpenHarmony的demo例子，以纯 JavaScript 实现的简明二进制对象表示 (CBOR) 数据格式 ( RFC 7049 )。
- [LibphoneNumber](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/LibphoneNumber) - 基于开源库libphonenumber-js开发OpenHarmony的demo例子，电话号码格式化和解析。

[返回目录](#目录)

### <a name="工具库"></a>工具库
- [lodashDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/lodashDemo) - 基于开源库lodash开发OpenHarmony的demo例子，提供模块性、性能和额外功能的现代JavaScript实用程序库。
- [jsDiffDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jsDiffDemo) - 基于开源库diff开发OpenHarmony的demo例子，一个 JavaScript 文本差异实现。
- [Easyrelpace](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/Easyrelpace) - 基于开源库leven，easy-replace等库开发OpenHarmony的demo例子，Compare Text替换字符串。
- [sanitize-html](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/sanitize-html) - 基于开源库sanitize-htm开发OpenHarmony的demo例子，sanitize-htm 提供了HTML清理API，支持HTML片段清理。
- [ahocorasick](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/ahocorasick) - 基于开源库ahocorasick开发OpenHarmony的demo例子，Aho-Corasick字符串搜索算法的实现，能够高效的进行字符串匹配。

[返回目录](#目录)

#### <a name="数学计算"></a>数学计算
- [mathjsDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/mathjsDemo) - 基于开源库mathjs，long，bignumber.js，matrix，math-base-assert等库开发OpenHarmony的demo例子，使用数学运算的示例，包含数字、大数、三角函数、字符串、和矩阵等数学功能。

[返回目录](#目录)

#### <a name="文件解析"></a>加解密算法
- [jszipDemo](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/jszipDemo) - 基于开源库jszip开发OpenHarmony的demo例子，支持加密或不加密zip格式的压缩/解压功能。

[返回目录](#目录)

### <a name="框架"></a>框架
- [RxJS](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/RxJS) - 基于开源库RxJS开发OpenHarmony的demo例子，通过使用可观察序列来合成异步和基于事件的程序。

[返回目录](#目录)

### <a name="其他"></a>其他
- [EventBus](https://gitee.com/openharmony-tpc/openharmony_tpc_samples/tree/master/EventBus) - 基于开源库eventbusjs开发OpenHarmony的demo例子，管理事件。

[返回目录](#目录)