1、操作步骤如下：

步骤1：先打包，程序在初始化显示界面。

步骤2：通过hdc命令行将的电子书（epub-book.epub）上传到文件夹”data/app/el2/100/base/cn.openharmony.epublib/haps/entry/files/”
```
# hdc_std file send  C:\Users\XXX\Desktop\epub-book.epub   data/app/el2/100/base/cn.openharmony.epublib/haps/entry/files/
```
步骤3：查看电子书文件权限

```
# hdc_std shell
# hdc_std cd data/app/el2/100/base/cn.openharmony.epublib/haps/entry/
# ls -l
total 8
drwxr-xr-x 2 20010036 20010036 4096 2017-08-11 09:43 cache
drwxr-xr-x 4 20010036 20010036 4096 2017-08-11 09:47 files
```
复制文件的权限；例如 20010036:20010036


步骤4：修改epub-book.epub 文件权限：

```
# hdc_std shell chown 20010036:20010036  /data/app/el2/100/base/cn.openharmony.epublib/haps/entry/files/epub-book.epub
```
步骤5： 点击Click 按钮。按钮变色，等待，内容显示

