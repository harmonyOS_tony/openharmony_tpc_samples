## epublib 单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

### 单元测试用例覆盖情况

| 接口名                                                             | 是否通过 | 备注 |
| ------------------------------------------------------------------ | -------- | ---- |
| getSpine()                                                            | pass     |
| setSpine()                                                            | pass     |
| getMetadata()                                                             | pass     |
| setMetadata()                                                             | pass     |
| getGuide()                                                             | pass     |
| addResource()                                                             | pass     |
| createResource()                                                             | pass     |
| createStrResource()                                                             | pass     |
| decode()                                                             | pass     |
| readEpubLazy()                                                             | pass     |
| readEpubFile()                                                             | pass     |
| readEpubToBook()                                                             | pass     |
| isEmpty()                                                             | pass     |
| getHref()                                                             | pass     |
| getByHref()                                                             | pass     |
| loadResources()                                                             | pass     |
| loadResourcesZip()                                                             | pass     |
| outFile()                                                             | pass     |
| addAuthor()                                                             | pass     |
| addTitle()                                                             | pass     |
| getAuthors()                                                             | pass     |
| setLanguage()                                                             | pass     |
| write()                                                             | pass     |

