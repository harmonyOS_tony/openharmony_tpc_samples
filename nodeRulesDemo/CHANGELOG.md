### 2.0.0

1.适配DevEco Studio:4.0 release Beta2(4.0.3.600) OpenHarmony SDK:API10(4.0.10.11)

### 1.0.0

1. 样例适配的系统版本是 OpenHarmony API 9.
2. 使用三方库 node-rules 版本支持@7.7.0.
3. 轻量级的正向链接规则引擎功能样例的实现.
