/**
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 *
 * This software is distributed under a license. The full license
 * agreement can be found in the file LICENSE in this distribution.
 * This software may not be copied, modified, sold or distributed
 * other than expressed in the named license agreement.
 *
 * This software is distributed without any warranty.
 */

export class RoomInfo {
     description: string = ''//描述
     subject: string = ''//主题
     occupants: string = ''//成员人数
     creationdate: string = ''//创建日期
}