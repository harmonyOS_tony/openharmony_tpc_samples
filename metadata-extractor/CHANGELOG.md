## 2.0.0
- DevEco Studio 4.0 Beta2（4.0.3.600）
- 适配SDK API10 (4.0.10.11)
- arkts 语法适配

## 1.0.3

1. 适配DevEco Studio 版本：3.1 Beta1（3.1.0.200），OpenHarmony SDK:API9（3.2.10.6）
3. fs替換fileio

## 1.0.2

- 补充六种格式的数据读取能力：adobe，exif ，wav，psd，tiff，quicktime
- 暴露更多的功能接口

## 1.0.1

- api8升级到api9，并转换为stage模型

## 1.0.0

  1. 从图像、视频和音频文件中提取元数据功能实现

- 遗留问题
1. xmp解析
   依赖adobe.xmp:xmpCore库